import { createApp } from 'vue'
import { createApolloProvider } from '@vue/apollo-option';
import { ApolloClient, InMemoryCache } from '@apollo/client/core'
import App from './App.vue'

const cache = new InMemoryCache()
const apolloClient = new ApolloClient({
  cache,
  uri: 'https://gitlab.com/api/graphql',
});
const apolloProvider = createApolloProvider({
  defaultClient: apolloClient,
});

const app = createApp(App);
app.use(apolloProvider);
app.mount('#app');
